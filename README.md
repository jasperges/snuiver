# Snuiver

Snuiver drives the pipeline for animation productions at
[Submarine](https://submarine.nl).

### Basic installation and usage instructions

The recommended way (at least for testing) to run Snuiver
([Avalon](https://github.com/getavalon) with
[Sonar](https://gitlab.com/jasperges/sonar) config and (experimental) support
for extra hosts) is in a virtual environment.

First you need to clone the repository:

```sh
$ git clone --recursive https://gitlab.com/jasperges/snuiver.git
```

#### Using `pipenv`
Now you can easily create a virtual environment with [`pipenv`](https://pipenv.readthedocs.io/en/latest/):

```sh
$ cd snuiver
[snuiver]$ pipenv install
```

Or if you also want the development requirements (just some linting and style
checking):

```sh
[snuiver]$ pipenv install --dev
```

Now copy `.env_template` to `.env` so `pipenv` will source it for the virtual
environment.

```sh
[snuiver]$ cp .env_template .env
```

Now edit `.env` so it fits your needs and points to the right paths. If you're
using `pipenv` you can easily find the the location of the virtual environment:

```sh
[snuiver]$ pipenv --venv
>>> /home/user/.local/share/virtualenvs/snuiver-fjhkd83K
```

Now make sure the executables for the applications in the `.toml` files in
`.../sonar/apps-config` are also on your path so they can be launched.

The last step is creating a project if you haven't already done so. Create the
folder in your `<project_root>` and initialize Avalon. Now (optionally) edit
the `.config.toml` and `.inventory.toml` configuration files. After that save
it to the database.

To do this you first need to activate the virtual environment.

```sh
[snuiver]$ pipenv shell
(snuiver) [snuiver]$ cd <project_root>
(snuiver) [<project_root>]$ mkdir testproject && cd testproject
(snuiver) [testproject]$ python -m avalon.inventory --init
(snuiver) [testproject]$ python -m avalon.inventory --save
```

Now you are ready to go! Go back to the `snuiver` directory and start the launcher.

```sh
(snuiver) [testproject]$ python -m launcher
```

#### Using `pip`

If you prefer not to use `pipenv`, you can simply create your own virtual
environment (or even skip this if you want), set the environment variables
(listed in `.env_template`) and install the dependencies and optionally the dev
dependencies with pip:

```sh
[snuiver]$ pip install -r requirements.txt
[snuiver]$ pip install -r requirements-dev.txt
```
<!-- vim:set ft=markdown.github: -->
